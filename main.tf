data "aws_organizations_organization" "this" {}



resource "aws_vpc" "this" {
    cidr_block = var.cidr_block

    tags = {
      Name = "Network"
    }
}

resource "aws_subnet" "public" {
    vpc_id = aws_vpc.this.id
    availability_zone = "us-east-1a"
    cidr_block = var.cidr_public

    tags = {
      "Name" = "Network_Public_Subnet_1"
    }
  
}

resource "aws_subnet" "public1" {
    vpc_id = aws_vpc.this.id
    availability_zone = "us-east-1b"
    cidr_block = var.cidr_public1

    tags = {
      "Name" = "Network_Public_Subnet_2"
    }
  
}

resource "aws_subnet" "public2" {
    vpc_id = aws_vpc.this.id
    availability_zone = "us-east-1c"
    cidr_block = var.cidr_public2

    tags = {
      "Name" = "Network_Public_Subnet_3"
    }
  
}

resource "aws_subnet" "private" {
    vpc_id = aws_vpc.this.id
    availability_zone = "us-east-1d"
    cidr_block = var.cidr_private

    tags = {
      "Name" = "Network_Private_Subnet_1"
    }
  
}

resource "aws_subnet" "private1" {
    vpc_id = aws_vpc.this.id
    availability_zone = "us-east-1e"
    cidr_block = var.cidr_private1

    tags = {
      "Name" = "Network_Private_Subnet_2"
    }
  
}

resource "aws_subnet" "private2" {
    vpc_id = aws_vpc.this.id
    availability_zone = "us-east-1f"
    cidr_block = var.cidr_private2

    tags = {
      "Name" = "Network_Private_Subnet_3"
    }
  
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.this.id

  tags = {
    "Name" = "main"
  }
  
}

resource "aws_route_table" "public" {
    vpc_id = aws_vpc.this.id

    route {
        cidr_block = var.igw_route
        gateway_id = aws_internet_gateway.internet_gateway.id
    }

    route {
      cidr_block = var.transit_gateway_route
      gateway_id = aws_ec2_transit_gateway.transit_gateway_example.id
    }

    tags = {
      "Name" = "PublicRT"
    }
    
} 

resource "aws_route_table_association" "public" {
    subnet_id = aws_subnet.public.id
    route_table_id = aws_route_table.public.id
 
}

resource "aws_route_table_association" "public1" {
    subnet_id = aws_subnet.public1.id
    route_table_id = aws_route_table.public.id
 
}

resource "aws_route_table_association" "public2" {
    subnet_id = aws_subnet.public2.id
    route_table_id = aws_route_table.public.id
 
}


resource "aws_route_table" "private" {
    vpc_id = aws_vpc.this.id

    
    route {
      cidr_block = var.igw_route
      gateway_id = aws_nat_gateway.this.id

    }

    route {
      cidr_block = var.transit_gateway_route
      gateway_id = aws_ec2_transit_gateway.transit_gateway_example.id
    }
    tags = {
      "Name" = "PrivateRT"
    }
    
} 

resource "aws_route_table_association" "private" {
    subnet_id = aws_subnet.private.id
    route_table_id = aws_route_table.private.id
 
}

resource "aws_route_table_association" "private1" {
    subnet_id = aws_subnet.private1.id
    route_table_id = aws_route_table.private.id
 
}

resource "aws_route_table_association" "private2" {
    subnet_id = aws_subnet.private2.id
    route_table_id = aws_route_table.private.id
 
}

resource "aws_eip" "nat_gateway" {
  vpc = true
}

resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.nat_gateway.id
  subnet_id = aws_subnet.public.id
  
}

resource "aws_ec2_transit_gateway" "transit_gateway_example" {
    auto_accept_shared_attachments = "enable"
    default_route_table_association = "enable"
    default_route_table_propagation = "enable"
    
  
}

resource "aws_ec2_transit_gateway_vpc_attachment" "this" {
    subnet_ids = [aws_subnet.public.id , aws_subnet.public1.id, aws_subnet.public2.id, aws_subnet.private.id, aws_subnet.private1.id, aws_subnet.private2.id]
    transit_gateway_id = aws_ec2_transit_gateway.transit_gateway_example.id
    vpc_id = aws_vpc.this.id  
  
}

resource "aws_ram_resource_share" "transit_gateway" {
  name = "sharing_transit_gateway"
  allow_external_principals = true
}

resource "aws_ram_resource_association" "example" {
  resource_arn       = aws_ec2_transit_gateway.transit_gateway_example.arn
  resource_share_arn = aws_ram_resource_share.transit_gateway.arn
}

resource "aws_ram_principal_association" "transit_gateway" {
  principal          = data.aws_organizations_organization.this.arn
  resource_share_arn = aws_ram_resource_share.transit_gateway.arn
}