variable "cidr_block" {
  type = string
  default = "10.0.0.0/16"
}

variable "cidr_public" {
    type = string
    default = "10.0.0.0/24"
  
}

variable "cidr_public1" {
    type = string
    default = "10.0.1.0/24"
  
}

variable "cidr_public2" {
    type = string
    default = "10.0.2.0/24"
  
}

variable "cidr_private" {
    type = string
    default = "10.0.3.0/24"
  
}

variable "cidr_private1" {
    type = string
    default = "10.0.4.0/24"
  
}

variable "cidr_private2" {
    type = string
    default = "10.0.5.0/24"
  
}

variable "igw_route" {
    type = string
    default = "0.0.0.0/0"
  
}

variable "transit_gateway_route" {
  type = string
  default = "10.0.0.0/8"
}